var nasabah = [
{
	nama : "Dadan",
	umur : 35,
	pemasukan : 3500000,
	rekening : 3
},
{
	nama : "Budi",
	umur : 22,
	pemasukan : 5000000,
	rekening : 2
},
{
	nama : "Dodi",
	umur : 60,
	pemasukan : 2000000,
	rekening : 1
},
{
	nama : "Amin",
	umur : 59,
	pemasukan : 1700000,
	rekening : 1
},
{
	nama : "Iwan",
	umur : 25,
	pemasukan : 4000000,
	rekening : 1
},
{
	nama : "Dodo",
	umur : 37,
	pemasukan : 5000000,
	rekening : 4
},
];

var nama,umur,pemasukan,rekening,submit;
var dataDist = [];

function setup()
{
	createCanvas(500,500);
	background(0);
	
	tampilForm();
}

function tampilForm()
{
	nama = createInput();
	nama.position(20,100);
	
	umur = createInput();
	umur.position(20,130);
	
	pemasukan = createInput();
	pemasukan.position(20,160);
	
	rekening = createInput();
	rekening.position(20,190);
	
	submit = createButton('SUBMIT');
	submit.position(20,220);
	submit.mousePressed(inputData);
}

function inputData()
{
	nasabah.push({
		nama : nama.value(),
		umur : parseInt(umur.value()),
		pemasukan : parseInt(pemasukan.value()),
		rekening : parseInt(rekening.value())
	});
	console.log("Berhasil Memasukan Nasabah");
	knn();
}

function tampilData()
{
	for( var i = 0 ; i < nasabah.length ; i++)
	{
		console.log(nasabah[i]);
	}
	
	for( var z = 1 ; z < dataDist.length ; z++){
		console.log("Nama : "+dataDist[z].nama+" , Kedekatan : "+dataDist[z].dekat);
	}
	
	console.log("Jadi nasabah "+nama.value()+" Mendapat pinjaman sebesar : Rp. "+ dataDist[1].jumlah);
}

function knn()
{
	var pBaru = parseInt(pemasukan.value());
	var rBaru = parseInt(rekening.value());
	
	for( var i = 0 ; i < nasabah.length ; i++)
	{
		var neig = dist(pBaru,rBaru,nasabah[i].pemasukan,nasabah[i].rekening);
		dataDist.push({
			nama : nasabah[i].nama,
			dekat : neig,
			jumlah : nasabah[i].pemasukan
		});
		//console.log(i);
	}
	dataDist.sort(dariJarak);
	
	function dariJarak(a,b)
	{
		return a.dekat - b.dekat;
	}
}